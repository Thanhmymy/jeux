// class Joueur {
//     joueur;

//     /**
//      * 
//      * @param {number} paramJoueur 
//      */
//     constructor(paramJoueur){
//         this.joueur = paramJoueur
//     }
// }


let button = document.querySelector('button')
let section = document.querySelector('#jeux')
let carre = document.querySelector('#carre')
let premiere = document.querySelector('#premiere')
let deuxieme = document.querySelector('#deuxieme')
let troisieme = document.querySelector('#troisieme')
let quatrieme = document.querySelector('#quatrieme')
let cinquieme = document.querySelector('#cinquieme')
let sixieme = document.querySelector('#sixieme')
let septieme = document.querySelector('#septieme')
let reset = document.querySelector("#reset")

let score1 = document.querySelector("#score1");
let score2 = document.querySelector("#score2");
let resultScore1 = document.querySelector("#resultScore1");
let resultScore2 = document.querySelector("#resultScore2");

let result1 = 0;
let result2 = 0;

//false = rouge, true = jaune
let couleur = false;

let tableau = [premiere, deuxieme, troisieme, quatrieme, cinquieme, sixieme, septieme];
let indice = 1
function boucle() {

    for (const item of tableau) {
        for (let x = 0; x < 6; x++) {
            let rond = document.createElement('div')
            rond.classList.add('rond')
            rond.id = indice
            indice++
            item.appendChild(rond)

        }
        carre.appendChild(item)

    }

}
boucle()

let cercle = document.querySelector(".rond")

let allPremiere = document.querySelectorAll('#premiere div');


let allDeuxieme = document.querySelectorAll('#deuxieme div');

let allTroisieme = document.querySelectorAll('#troisieme div');

let allQuatrieme = document.querySelectorAll('#quatrieme div');

let allCinquieme = document.querySelectorAll('#cinquieme div');

let allSixieme = document.querySelectorAll('#sixieme div');

let allSeptieme = document.querySelectorAll('#septieme div');

let tableauDeux = [allPremiere,allDeuxieme,allTroisieme,allQuatrieme,allCinquieme,allSixieme,allSeptieme]



// Inserer les jetons dans les emplacements.



premiere.addEventListener('click', function () {
    for (let i = allPremiere.length - 1; i >= 0; i--) {
        if (allPremiere[i].style.backgroundColor == "") {
            if (couleur == false) {
                allPremiere[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test()
                return;
            } else {
                allPremiere[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test()
                return;
            }
        }
    }
})

deuxieme.addEventListener('click', function () {
    for (let i = allDeuxieme.length - 1; i >= 0; i--) {
        if (allDeuxieme[i].style.backgroundColor == "") {
            if (couleur == false) {
                allDeuxieme[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test();
                return;
            } else {
                allDeuxieme[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test();
                return;
            }
        }

    }
})

troisieme.addEventListener('click', function () {
    for (let i = allTroisieme.length - 1; i >= 0; i--) {
        if (allTroisieme[i].style.backgroundColor == "") {
            if (couleur == false) {
                allTroisieme[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test();
                return;
            } else {
                allTroisieme[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test();
                return;
            }
        }

    }
})

quatrieme.addEventListener('click', function () {
    for (let i = allQuatrieme.length - 1; i >= 0; i--) {
        if (allQuatrieme[i].style.backgroundColor == "") {
            if (couleur == false) {
                allQuatrieme[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test();
                return;
            } else {
                allQuatrieme[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test();
                return;
            }
        }

    }
})

cinquieme.addEventListener('click', function () {
    for (let i = allCinquieme.length - 1; i >= 0; i--) {
        if (allCinquieme[i].style.backgroundColor == "") {
            if (couleur == false) {
                allCinquieme[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test();
                return;
            } else {
                allCinquieme[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test();
                return;
            }
        }

    }
})

sixieme.addEventListener('click', function () {
    for (let i = allSixieme.length - 1; i >= 0; i--) {
        if (allSixieme[i].style.backgroundColor == "") {
            if (couleur == false) {
                allSixieme[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test();
                return;
            } else {
                allSixieme[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test();
                return;
            }
        }

    }
})

septieme.addEventListener('click', function () {
    for (let i = allSeptieme.length - 1; i >= 0; i--) {
        if (allSeptieme[i].style.backgroundColor == "") {
            if (couleur == false) {
                allSeptieme[i].style.backgroundColor = '#EF9E9F';
                couleur = true;
                test();
                return;
            } else {
                allSeptieme[i].style.backgroundColor = '#F6EA8C';
                couleur = false;
                test();
                return;
            }
        }

    }
})




//bouton reset, remettant les cases vides
reset.addEventListener('click',()=>{
    for (const element of tableauDeux) {
        for (let i=0; i<element.length;i++) {
                element[i].style.backgroundColor = "";
        }
    }
});

//On test si c'est gagnant ou pas

function test(){
    let compteurGagnant = 0;
    for (const element of tableauDeux) {
        for (let i = element.length - 1; i >= 0; i--) {
            if (element[i].style.backgroundColor !== "") {
                testCase(element[i].id);
                
            } 
        }
        
    }
}


function testVertical(idBase){
    idBase = parseInt(idBase);
    let couleurBase = nouveau(idBase);
    let premierHaut=parseInt(idBase)-1;
    let deuxiemeHaut=parseInt(idBase)-2;
    let troisiemeHaut=parseInt(idBase)-3;
    let tableHaut = [premierHaut,deuxiemeHaut,troisiemeHaut];
    let testGagnant = true;
    //Cette condition montre les endroits dans la grille où il est inutile de tester
    //par exemple en dessous de 4 il n'est pas possible d'avoir un puissance 4 verticalement
    if(idBase < 4 || idBase>6 && idBase<10 || idBase>12 && idBase<16 || idBase>18 && idBase<22||idBase>24 && idBase<28||idBase>30 && idBase<34
        || idBase>36 && idBase<40 ){

    }else{
        tableHaut.forEach(element => {
            if(nouveau(element) !== couleurBase){
                testGagnant = false;
            }
        });
        if(testGagnant == true){
            testCouleurGagnante(couleurBase);
            return couleurBase;
        }
    }
}

function testDiagonaleDroite(idBase){
        idBase = parseInt(idBase);
        let couleurBase = nouveau(idBase);
        let premiereDiagonale=parseInt(idBase)+5;
        let deuxiemeDiagonale=parseInt(idBase)+10;
        let troisiemeDiagonale=parseInt(idBase)+15;
        let tableDiagonale = [premiereDiagonale,deuxiemeDiagonale,troisiemeDiagonale];
        let testGagnant = true;
        if(idBase < 4 || idBase>6 && idBase<10 || idBase>12 && idBase<16 || idBase>18 && idBase<22||idBase>24 && idBase<28||idBase>30 && idBase<34
            || idBase>36 && idBase<40||idBase>24 ){
    
        }else{
            tableDiagonale.forEach(element => {
                if(nouveau(element) !== couleurBase){
                    testGagnant = false;
                }
            });
            if(testGagnant == true){
                testCouleurGagnante(couleurBase);
                return couleurBase;
            }
        }
}

function testDiagonaleGauche(idBase){
    idBase = parseInt(idBase);
    let couleurBase = nouveau(idBase);
    let premiereDiagonale=parseInt(idBase)-7;
    let deuxiemeDiagonale=parseInt(idBase)-14;
    let troisiemeDiagonale=parseInt(idBase)-21;
    let tableDiagonale = [premiereDiagonale,deuxiemeDiagonale,troisiemeDiagonale];
    let testGagnant = true;
    if(idBase < 4 || idBase>6 && idBase<10 || idBase>12 && idBase<16 || idBase>18 && idBase<22||idBase>24 && idBase<28||idBase>30 && idBase<34
        || idBase>36 && idBase<40){
            

    }else{
        tableDiagonale.forEach(element => {
            if(nouveau(element) !== couleurBase){
                testGagnant = false;
            }
        });
        if(testGagnant == true){
            testCouleurGagnante(couleurBase);
            return couleurBase;
        }
    }
}

//cette fonction permet de retrouver une case avec un id et verifie si il y a une couleur ou pas et laquelle
//a= id qu'on recherche
function nouveau(a){
    //ex : la premiere case de l'element c'est element [0]=> une case
    for (const element of tableauDeux) {
        for (let i=0; i<element.length;i++) {
            if (element[i].id == a){
                //ici on test si noter case est pas vide
                if(element[i].style.backgroundColor !== ""){
                    //Ici on test si la case actuelle est de la couleur "rouge"                    
                    if (element[i].style.backgroundColor === "rgb(239, 158, 159)"){
                        return 'red'
                    }else {
                        return 'yellow'
                    }
                }else{
                    return'il n\'y a pas de couleur'
                }
                
            }
        }
    
}
}


//Cette fonction permet de tester à droite de l'élément
function testHorizontale(idBase) {
    //parseInt permet de convertir la variable en entier 
    idBase = parseInt(idBase);

    let couleurBase = nouveau(idBase);

    let premiereDroite=parseInt(idBase)+6;

    let deuxiemeDroite=parseInt(idBase)+12;

    let troisiemeDroite=parseInt(idBase)+18;
    
    let tableDroite = [premiereDroite,deuxiemeDroite,troisiemeDroite];

    //Un boolean pour tester si il y'a un intrus parmis les 3 jetons suivant
    let testGagnant = true;

    //Ca ne sert à rien de tester les jetons au dessus de 24
    if(idBase <=24){
        //On parcours le tableau

        tableDroite.forEach(element => {
            if(nouveau(element) !== couleurBase){
                testGagnant = false;
            }
        });
        if(testGagnant == true){
            testCouleurGagnante(couleurBase);
            return couleurBase;
        }
    }
}

//Cette fonction va gérer la partie de la couleur récuperer en paramètre
function testCouleurGagnante(couleur){
    if(couleur == 'red'){
        alert("GAGNANT ROUGE !");
        result2 ++;
        resultScore2.textContent = result2;
    }else if(couleur =='yellow'){
        alert("GAGNANT JAUNE !");
        result1++
        resultScore1.textContent = result1;
    }
}

//Dans cette fonction on met tous les tests qu'on puisse faire à une case donnée
function testCase(id){
    testHorizontale(id);
    testVertical(id);
    testDiagonaleDroite(id);
    testDiagonaleGauche(id)
}

