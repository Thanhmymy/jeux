# Project Game

Pendant la formation Dev Web et Mobile nous avons eu comme projet de creer un jeux vidéo en JS.

## _Objectif:_

- Utilisation des objets Javascript d'une manière ou d'une autre
- Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.), l'idée est de travailler la séparation des données et de l'affichage.

## _Concept du jeu:_

J'ai repris le concept du jeu 'Puissance 4'. Le jeu consiste à aligner 4 ronds de la même couleur de façon horizontale,verticale ou même diagonale. Ce jeu se joue avec 2 joueurs, sur un seul écran. 

## _Langage utilisé:_

Ce jeu est codé en HTML/CSS/JS.

## _Maquette:_
![alt text](public/Assets/2021-05-31.png)

## _Lien:_
https://thanhmymy.gitlab.io/jeux